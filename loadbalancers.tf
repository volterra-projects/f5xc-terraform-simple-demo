locals {
  public_dns_name = format("%s-%s", var.resource_prefix, var.public_dns_name)
}

resource "volterra_http_loadbalancer" "app_public_lb" {

  lifecycle {
    ignore_changes = [labels]
  }
  name        = format("%s-app-public-lb", var.resource_prefix)
  namespace   = var.namespace
  description = format("Public Http loadbalancer object for %s service", var.resource_prefix)
  domains     = [local.public_dns_name]

  advertise_on_public_default_vip = true
  https_auto_cert {
    http_redirect         = true
    no_mtls               = true
    add_hsts              = false
    default_header        = true
    enable_path_normalize = true
    tls_config {
      default_security = true
    }
    port = "443"
  }

  default_route_pools {
    pool {
      name      = volterra_origin_pool.origin_pool.name
      namespace = var.namespace
    }
    weight   = 1
    priority = 1
  }
  round_robin = true

  routes {
    direct_response_route {
      path {
        path = "/hello"
      }
      route_direct_response {
        response_body = "Hello World"
        response_code = "200"
      }
    }
  }

  ## API Discovery
  disable_api_definition = true
  enable_api_discovery {
    enable_learn_from_redirect_traffic = true
  }

  ## IP Reputation 
  enable_ip_reputation {
    ip_threat_categories = ["BOTNETS", "SCANNERS", "DENIAL_OF_SERVICE"]
  }

  ## DDOS
  enable_ddos_detection {
    enable_auto_mitigation {
      block = true
    }
  }

  ddos_mitigation_rules {
    metadata {
      name        = "ddos-mitigation-rule-tls-fp"
      description = "Block TLS fingerprint based DDOS from ADWARE and TRICKBOT"
    }
    block = true
    ddos_client_source {
      tls_fingerprint_matcher {
        classes = ["ADWARE", "TRICKBOT"]
      }
    }
  }

  ## Malicious Users
  enable_malicious_user_detection = true

  enable_challenge {
    default_captcha_challenge_parameters = true
    default_js_challenge_parameters      = true
    default_mitigation_settings          = true
  }

  ## Bot Defense
  bot_defense {
    regional_endpoint = "EU"
    policy {
      javascript_mode = "ASYNC_JS_NO_CACHING"
      js_insert_all_pages {
        javascript_location = "AFTER_HEAD"
      }
      protected_app_endpoints {
        metadata {
          name = "root-endpoint"
        }
        web = true
        domain {
          exact_value = local.public_dns_name
        }
        path {
          prefix = "/"
        }
        protocol     = "BOTH"
        http_methods = ["METHOD_GET_DOCUMENT"]
        mitigation {
          flag {
            no_headers = true

          }
        }
        mitigate_good_bots = true
      }
    }
  }

  ## General security
  disable_rate_limit              = true
  disable_trust_client_ip_headers = true
  user_id_client_ip               = true
  add_location                    = true

  ## Service Policies
  active_service_policies {
    policies {
      name      = volterra_service_policy.blocking_service_policy.name
      namespace = var.namespace
    }
    policies {
      name      = "ves-io-allow-all"
      namespace = "shared"
      tenant    = "ves-io"
    }
  }

  ## WAF
  app_firewall {
    name      = volterra_app_firewall.waf.name
    namespace = var.namespace
  }

  waf_exclusion_rules {
    metadata {
      name        = "exclude-xss-parameter"
      description = "Exclude XSS parameter block from WAF policy"
    }
    exact_value = local.public_dns_name
    methods     = ["ANY"]
    app_firewall_detection_control {
      exclude_signature_contexts {
        signature_id = 200000098
        context      = "CONTEXT_PARAMETER"
      }
      exclude_signature_contexts {
        signature_id = 200001475
        context      = "CONTEXT_PARAMETER"
      }

    }
  }

  waf_exclusion_rules {
    metadata {
      name        = "exclude-xss-url"
      description = "Exclude XSS URL block from WAF policy"
    }
    exact_value = local.public_dns_name
    methods     = ["ANY"]
    app_firewall_detection_control {
      exclude_signature_contexts {
        signature_id = 200000093
        context      = "CONTEXT_URL"
      }
      exclude_signature_contexts {
        signature_id = 200000099
        context      = "CONTEXT_URL"
      }
    }
  }

}

data "volterra_http_loadbalancer_state" "public-lb-state" {
  name      = volterra_http_loadbalancer.app_public_lb.name
  namespace = var.namespace
}

