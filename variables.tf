variable "api_p12_file" {
  type        = string
  description = "Volterra API p12 file path"
}

variable "api_url" {
  type        = string
  description = "Volterra tenant api url"
  default     = "https://yourtenant.console.ves.volterra.io/api"
}

variable "resource_prefix" {
  type        = string
  description = "Object Prefix"
}

variable "namespace" {
  type        = string
  description = "Volterra namespace to use"
}

variable "public_dns_name" {
  type        = string
  description = "app public domain name, resource_prefix will be added"
}

variable "origin_dns_name" {
  type        = string
  description = "origin app public domain name"
}
