resource "volterra_origin_pool" "origin_pool" {
  name                   = format("%s-origin", var.resource_prefix)
  namespace              = var.namespace
  description            = format("Origin pool pointing to origin server public domain")
  loadbalancer_algorithm = "ROUND ROBIN"
  healthcheck {
    name      = volterra_healthcheck.http_hc.name
    namespace = var.namespace
  }
  origin_servers {
    public_name {
      dns_name = var.origin_dns_name
    }
  }
  automatic_port = true
  use_tls {
    tls_config {
      default_security = true
    }
  }
  endpoint_selection = "LOCAL_PREFERRED"
}
