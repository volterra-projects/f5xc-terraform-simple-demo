resource "volterra_app_firewall" "waf" {
  name        = "${var.resource_prefix}-waf"
  description = format("Web application firewall policy for %s", var.resource_prefix)
  namespace   = var.namespace

  // One of the arguments from this list "allow_all_response_codes allowed_response_codes" must be set
  allow_all_response_codes = true

  // One of the arguments from this list "default_anonymization custom_anonymization disable_anonymization" must be set
  default_anonymization = true

  // One of the arguments from this list "use_default_blocking_page blocking_page" must be set
  blocking_page {
    blocking_page = "string:///PGgxPkJMT0NLM0Q8L2gxPg0KPGgyPkNvbnRhY3QgdGhlIGFkbWluIHdpdGgge3tyZXF1ZXN0X2lkfX08L2gyPg=="
    response_code = "OK"
  }

  // One of the arguments from this list "default_bot_setting bot_protection_setting" must be set
  bot_protection_setting {
    malicious_bot_action  = "BLOCK"
    suspicious_bot_action = "REPORT"
    good_bot_action       = "REPORT"
  }

  // One of the arguments from this list "default_detection_settings detection_settings" must be set
  #default_detection_settings = true
  detection_settings {
    signature_selection_setting {
      attack_type_settings {
        disabled_attack_types = ["ATTACK_TYPE_DETECTION_EVASION", "ATTACK_TYPE_FORCEFUL_BROWSING"]
      }
      only_high_accuracy_signatures = true
    }
    enable_suppression      = true
    enable_threat_campaigns = true
    stage_new_and_updated_signatures {
      staging_period = 7
    }
    violation_settings {
      disabled_violation_types = ["VIOL_HTTP_PROTOCOL_BAD_HTTP_VERSION", "VIOL_EVASION_APACHE_WHITESPACE"]
    }
  }



  // One of the arguments from this list "use_loadbalancer_setting blocking monitoring" must be set
  blocking = true
}
