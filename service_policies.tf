resource "volterra_service_policy" "blocking_service_policy" {
  name        = format("%s-curl-block-policy", var.resource_prefix)
  description = format("Blocking service policy for %s", var.resource_prefix)
  namespace   = var.namespace

  algo = "" # No idea what this does -> (Required) - DENY_OVERRIDES Rules with a DENY action are evaluated prior to rules with an ALLOW action (String).

  rule_list {
    rules {
      metadata {
        name = "block-curl"
      }
      spec {
        action     = "DENY"
        any_client = true
        headers {
          name = "user-agent"
          item {
            regex_values = ["(?i)^.*curl.*$"]
          }
        }
        waf_action {
          none = true
        }
      }
    }
  }
}

