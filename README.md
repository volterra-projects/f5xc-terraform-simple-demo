# F5XC Terraform simple demo

This Terraform script creates a HTTP Loadbalancer in XC Regional edges with some sample configuration for security and proxies traffic to some public DNS endpoint.


See https://registry.terraform.io/providers/volterraedge/volterra/latest/docs for volterra (now F5XC) terraform provider docs.

## Demo steps
1. Create a F5XC api credential (see https://docs.cloud.f5.com/docs/how-to/user-mgmt/credentials)
2. Download and put the .p12 file in the **/creds** folder
3. Create an **VES_P12_PASSWORD** environment variable with your credential password entered in F5 XC GUI/API during creation:

    Example (Linux):
    ```
    read -s VES_P12_PASSWORD && export VES_P12_PASSWORD
    ```
4. Create a **terraform.tfvars** file in the main folder
    
    Example

    ```
    api_p12_file = "./creds/<YOUR FILE>.p12"
    api_url       = "https://<TENANT NAME>.console.ves.volterra.io/api"

    resource_prefix = "<INITIALS>-simple-demo"
    namespace = "<VOLTERRA NAMESPACE>"

    public_dns_name = "<PUBLIC DOMAIN NAME FOR XC LOADBALANCER>"
    origin_dns_name = "<PUBLIC DOMAIN NAME FOR ORIGIN SERVER>"
    ```
5. `terraform init`
6. `terraform apply`

> `terraform destroy` when done.


